//
//  ViewController.swift
//  Project18
//
//  Created by Роман Хоменко on 23.06.2022.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // "debugging" with prints
        print("In viewDidLoad")
        
        // debugging with asserts
        assert(1 == 1, "Math failure")
//        assert(1 == 2, "Math failure")
        
        assert(mySlowMethod() == true, "The slow method returned false")
        
        // test breakpoints
        for i in 1...100 {
            print("Dot number \(i)")
            
        }
        // test vieew debbuging
    }

    func mySlowMethod() -> Bool { true }
}

